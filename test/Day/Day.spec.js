import { createLocalVue, mount } from '@vue/test-utils'
import Day from '@/components/Day/Day'
import Vuetify from 'vuetify'
import CompositionApi from '@vue/composition-api'

describe('Day', () => {
  const localVue = createLocalVue()
  let vuetify

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  test('is a Vue instance', () => {
    const wrapper = mount(Day,{
      localVue,
      vuetify,
      CompositionApi
    })
    expect(wrapper.vm).toBeTruthy()
  })
})
