import colors from 'vuetify/es5/util/colors'
export default {
  ssr: false,
  target: 'static',
  head: {
    titleTemplate: '%s - calendar',
    title: 'calendar',
    htmlAttrs: {
      lang: 'pl'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  css: [
    'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css',
    'vue-snotify/styles/material.scss',
    '@/assets/main.scss',
  ],
  plugins: [
    '~/plugins/Vuelidate',
    '~/plugins/Snotify',
    '~/plugins/Axios',
    '~/plugins/Dayjs',
  ],
  components: true,
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/vuetify',
    '@nuxtjs/composition-api/module',
  ],
  modules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/axios',
    // '@nuxtjs/dayjs',
  ],
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  env: {
    OPEN_WEATHER_API_KEY: process.env.OPEN_WEATHER_API_KEY
  },
  build: {
    transpile: ['vuex-composition-helpers']
  }
}
