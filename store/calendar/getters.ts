import { GetterTree } from 'vuex'
import DayEvent from '~/models/DayEvent'
import { State } from '~/store/calendar/state'
export enum GetterType {
  getSelectedDay = 'GET_SELECTED_DAY',
  getSelectedEvent = 'GET_SELECTED_EVENT',
  getEventListModal = 'GET_EVENT_LIST_MODAL',
  getEventList = 'GET_EVENT_LIST',
}
export type Getters = {
  [GetterType.getSelectedDay](state: State): string | null
  [GetterType.getSelectedEvent](state: State): DayEvent | null
  [GetterType.getEventListModal](state: State): boolean
  [GetterType.getEventList](state: State): Array<DayEvent>
}
export const getters = {
  [GetterType.getSelectedDay]: (state: State) => {
    return state.selectedDay 
  },
  [GetterType.getSelectedEvent]: (state: State) => {
    return state.selectedEvent
  },
  [GetterType.getEventListModal]: (state: State): boolean => {
    return state.eventListModal
  },
  [GetterType.getEventList]: (state: State) => {
    return state.eventList
  },
}
export default getters