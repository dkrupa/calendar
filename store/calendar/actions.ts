import { ActionContext, ActionTree } from 'vuex'
import { Mutations, MutationType } from '~/store/calendar/mutations'
import { State } from '~/store/calendar/state'
import DayEvent from '~/models/DayEvent'
const EVENT_LIST_STORAGE = 'EventList'
export enum ActionType {
  getEventListFromStorage = 'GET_EVENT_LIST_FROM_STORAGE',
  saveEventListToStorage = 'SAVE_EVENT_LIST_TO_STORAGE',
  addEventToStorage = 'ADD_EVENT_TO_STORAGE',
  updateEventInStorage = 'UPDATE_EVENT_IN_STORAGE',
  removeEventFromStorage = 'REMOVE_EVENT_FROM_STORAGE',
}
type ActionAugments = Omit<ActionContext<State, State>, 'commit'> & {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>
}
export type Actions = {
  [ActionType.getEventListFromStorage](context: ActionAugments): void
  [ActionType.saveEventListToStorage](context: ActionAugments): void
  [ActionType.addEventToStorage](context: ActionAugments, event: DayEvent): void
  [ActionType.updateEventInStorage](context: ActionAugments, event: DayEvent): void
  [ActionType.removeEventFromStorage](context: ActionAugments, id: string): void
}
export const actions = {
  async [ActionType.getEventListFromStorage]({ commit }: ActionAugments) {
    const jsonList = localStorage.getItem(EVENT_LIST_STORAGE)
    const eventList = jsonList == null ? [] : JSON.parse(jsonList)
    commit(MutationType.setEventList, eventList)
  },
  async [ActionType.saveEventListToStorage]({ state }: ActionAugments) {
    const selectedDay = state.selectedDay
    if(selectedDay == null) return
    window.localStorage.setItem(EVENT_LIST_STORAGE, JSON.stringify(selectedDay))
  },
  async [ActionType.addEventToStorage]({ state, commit }: ActionAugments, event: DayEvent) {
    commit(MutationType.addEvent, event)
    window.localStorage.setItem(EVENT_LIST_STORAGE, JSON.stringify(state.eventList))
  },
  async [ActionType.updateEventInStorage]({ state, commit }: ActionAugments, event: DayEvent) {
    commit(MutationType.updateEvent, event)
    window.localStorage.setItem(EVENT_LIST_STORAGE, JSON.stringify(state.eventList))
  },
  async [ActionType.removeEventFromStorage]({ state, commit }: ActionAugments, id: string) {
    commit(MutationType.removeEvent, id)
    window.localStorage.setItem(EVENT_LIST_STORAGE, JSON.stringify(state.eventList))
  },
}

export default actions