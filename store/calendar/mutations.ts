import { MutationTree } from 'vuex'
import DayEvent from '~/models/DayEvent'
import { State } from '~/store/calendar/state'
import dayjs from 'dayjs'
import 'dayjs/locale/pl'
dayjs.locale('pl')
export enum MutationType {
  selectDay = 'SELECT_DAY',
  showEventListModal = 'SHOW_EVENT_LIST_MODAL',
  hideEventListModal = 'HIDE_EVENT_LIST_MODAL',
  saveEvent = 'SAVE_EVENT',
  updateEvent = 'UPDATE_EVENT',
  selectEvent = `SELECT_EVENT`,
  clearSelectedEvent = 'CLEAR_SELECTED_EVENT',
  addEventToStorage = 'ADD_EVENT_TO_STORAGE',
  removeEventFromStorage = 'REMOVE_EVENT_FROM_STORAGE',
  updateEventInStorage = 'UPDATE_EVENT_IN_STORAGE',
  addEvent = 'ADD_EVENT',
  goNextDay = 'GO_NEXT_DAY',
  goPrevDay = 'GO_PREV_DAY',
  setEventList = 'SET_EVENT_LIST',
  removeEvent = 'REMOVE_EVENT'
}
export type Mutations = {
  [MutationType.selectDay](state: State, day: string): void
  [MutationType.selectEvent](state: State, event: DayEvent): void
  [MutationType.clearSelectedEvent](state: State): void
  [MutationType.showEventListModal](state: State): void
  [MutationType.hideEventListModal](state: State): void
  [MutationType.addEvent](state: State, event: DayEvent): void
  [MutationType.updateEvent](state: State, event: DayEvent): void
  [MutationType.removeEvent](state: State, id: string): void
  [MutationType.setEventList](state: State, eventList: Array<Event>): void
  [MutationType.goNextDay](state: State): void
  [MutationType.goPrevDay](state: State): void
}
export const mutations = {
    [MutationType.selectDay]: (state: State, day: string) => {
      state.selectedDay = day
    },
    [MutationType.selectEvent]: (state: State, event: DayEvent) => {
      state.selectedEvent = event
    },
    [MutationType.clearSelectedEvent]: (state: State) => {
      state.selectedEvent = null
    },
    [MutationType.showEventListModal]: (state: State) => {
      state.eventListModal = true
    },
    [MutationType.hideEventListModal]: (state: State) => {
      state.eventListModal = false
    },
    [MutationType.addEvent]: (state: State, event: DayEvent) => {
      if(event == null) return
      if(!Array.isArray(state.eventList)) state.eventList = []
      state.eventList.push({...event})
    },
    [MutationType.updateEvent]: (state: State, event: DayEvent) => {
      if(!Array.isArray(state.eventList)) return
      let eventToUpdate = state.eventList.find(e => e.id === event.id)
      if(eventToUpdate == null) return
      Object.assign(eventToUpdate,event)
    },
    [MutationType.removeEvent]: (state: State, id: string) => {
      if(id == null) return
      if(!Array.isArray(state.eventList)) return
      const index = state.eventList.findIndex(e => e.id === id)
      if(index === -1) return 
      state.eventList.splice(index, 1)
    },
    [MutationType.setEventList]: (state: State, eventList: Array<DayEvent>) => {
      state.eventList = eventList
    },
    [MutationType.goNextDay]: (state: State) => {
      state.selectedDay = dayjs(state.selectedDay).add(1,'day').format('YYYY-MM-DD')
    },
    [MutationType.goPrevDay]: (state: State) => {
      state.selectedDay = dayjs(state.selectedDay).subtract(1,'day').format('YYYY-MM-DD')
    },
  }


export default mutations