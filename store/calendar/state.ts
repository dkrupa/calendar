import DayEvent from "~/models/DayEvent"
export type State = {
  selectedDay: string | null,
  selectedEvent: DayEvent | null,
  eventListModal: boolean,
  editEventModal: boolean,
  eventList: Array<DayEvent>,
}
export const state: State = {
  selectedDay: null,
  selectedEvent: null,
  eventListModal: false,
  editEventModal: false,
  eventList: [],
}
export default state