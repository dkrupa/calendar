export interface WeatherData {
  temp: string
  feels_like: string
  pressure: string
  humidity: string  
}

export default class Weather implements WeatherData{
  temp: string
  feels_like: string
  pressure: string
  humidity: string 

  constructor(data?: WeatherData){
    this.temp = data != null ? data.temp : ''
    this.feels_like = data != null ? data.feels_like : ''
    this.pressure = data != null ? data.pressure : ''
    this.humidity = data != null ? data.humidity : ''
  }
}