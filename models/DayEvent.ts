export default class DayEvent {
  id: string
  startDate: string
  endDate: string
  description: string
  
  constructor(id: string = '', startDate: string = '', endDate: string = '', descritpion: string = ''){
    this.id = id
    this.startDate = startDate
    this.endDate = endDate
    this.description = descritpion
  }

}