import { Plugin } from '@nuxt/types'
import { AxiosError } from 'axios'
import { initializeAxios } from '~/utils/api'

const accessor: Plugin = ({ error, app: { $axios } }) => {
  initializeAxios($axios)
   
  $axios.onError((error: AxiosError<any>) => {
    const response = {
      errorApi: true, 
      code: error.code, 
      errorMessage: error.toString(),
    }
    return response
  })
}

export default accessor