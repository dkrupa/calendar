import { Plugin } from '@nuxt/types'
import dayjs from 'dayjs'
import {Dayjs} from 'dayjs'
import isBetween from 'dayjs/plugin/isBetween'
declare module 'vue/types/vue' {
  interface Vue {
    $dayjs(date?: dayjs.ConfigType, option?: dayjs.OptionType, locale?: string): Dayjs
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $dayjs(date?: dayjs.ConfigType, option?: dayjs.OptionType, locale?: string): Dayjs
  }
  interface Context {
    $dayjs(date?: dayjs.ConfigType, option?: dayjs.OptionType, locale?: string): Dayjs
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    $dayjs(date?: dayjs.ConfigType, option?: dayjs.OptionType, locale?: string): Dayjs
  }
}

const myPlugin: Plugin = (_context, inject) => {
  inject('dayjs', (date?: dayjs.ConfigType, option?: dayjs.OptionType, locale?: string): Dayjs => { 
    dayjs.extend(isBetween)
    dayjs.locale('pl')
    return dayjs(date)
  })
}

export default myPlugin